import {Component, OnInit} from '@angular/core';
import {formatDate} from "@angular/common";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  public days = [];
  constructor() {}
    ngOnInit() {
        const today = new Date();
        this.setDaysArray(today, 100);
    }
    setDaysArray(date, noOfDays) {
        // Start yesterday to ensure picture is released
        date.setDate(date.getDate() - 1);

        for (let n = 0; n < noOfDays; n++) {
            this.days.push({
                apiDate: this.getApiDate(date),
                displayDate: this.createDisplayDate(date)
            });
            date.setDate(date.getDate() - 1);
        }
        console.log(this.days);

    }

    getApiDate(date) {
        return formatDate(date, 'yyyy-MM-dd', 'en_GB');
    }
    createDisplayDate(date) {
        return formatDate(date, 'MMM d yyyy', 'en_GB');
    }
}
