import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  private date: string;
  public detailData$;
  constructor(
    private activatedRoute: ActivatedRoute,
    private http$: HttpClient) { }

  ngOnInit() {
    this.date = this.activatedRoute.snapshot.paramMap.get('date');
    this.detailData$ = this.http$.get('https://api.nasa.gov/planetary/apod?api_key=Fg8TtYfOzIZvqhOwIx83N9TVo4jWsjMiPT8pz7rA&date=' + this.date);
  }
}
